import {
	increment,
	decrement,
	incrementByValue,
	decrementByValue,
} from "../redux/features/counter/counterSlice";

import { useDispatch, useSelector } from "react-redux";

const Counter = () => {
	const dispatch = useDispatch(); // dispatch
	const { count } = useSelector((state) => state.counter);

	return (
		<div className="border-2 border-purple-500 bg-purple-200 pb-10 w-[700px] mx-auto mt-10 rounded-md text-purple-700">
			<h1 className="text-3xl font-semibold text-center my-10">{count}</h1>
			<div className="flex items-center justify-center gap-6 text-white">
				<button
					onClick={() => dispatch(decrement())}
					className="bg-purple-500 hover:bg-purple-700 py-3 px-5 rounded-md font-semibold"
				>
					Decrement
				</button>
				<button
					onClick={() => dispatch(decrementByValue(5))}
					className="bg-purple-500 hover:bg-purple-700 py-3 px-5 rounded-md font-semibold"
				>
					Decrement By 5
				</button>
				<button
					onClick={() => dispatch(increment())}
					className="bg-purple-500 hover:bg-purple-700 py-3 px-5 rounded-md font-semibold"
				>
					Increment
				</button>
				<button
					onClick={() => dispatch(incrementByValue(5))}
					className="bg-purple-500 hover:bg-purple-700 py-3 px-5 rounded-md font-semibold"
				>
					Increment By 5
				</button>
			</div>
		</div>
	);
};

export default Counter;
